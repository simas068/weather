import {
  BOUNDS_WEATHER_LOADED
} from './constants'

export function boundsWeatherLoaded (data) {
  return {
    type: BOUNDS_WEATHER_LOADED,
    payload: data
  }
}
