import {
  BOUNDS_WEATHER_LOADED
} from './constants'
import update from 'immutability-helper'

const initialState = {
  bounds: {
    list: []
  }
}

export default function weatherReducer (state = initialState, action) {
  switch (action.type) {
    case BOUNDS_WEATHER_LOADED:
      return update(state, {
        $merge: {
          bounds: action.payload.list ? action.payload : state.bounds
        }
      })
    default:
      return state
  }
}
