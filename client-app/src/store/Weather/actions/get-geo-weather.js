import queryString from 'query-string'

export function getGeoWeather (lat, lng) {
  return (dispatch, getState) => {
    const params = {
      lat: lat,
      lon: lng,
      units: 'metric',
      appid: __OWM_APP_ID__
    }
    const query = queryString.stringify(params)

    return fetch(`${__OWM_API_URL__}/data/2.5/weather?${query}`)
      .then(response => {
        if (response.status === 200) {
          return response.json()
        } else {
          throw new Error(response.statusText)
        }
      })
      .then(response => {
        return response
      })
      .catch(error => {
        console.log(error)
      })
  }
}
