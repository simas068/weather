import { boundsWeatherLoaded } from '../utils'
import queryString from 'query-string'

export function getBoundsWeather (bounds, zoom) {
  return (dispatch, getState) => {
    const params = {
      bbox: `${bounds.b.b},${bounds.f.b},${bounds.b.f},${bounds.f.f},${zoom}`,
      appid: __OWM_APP_ID__
    }
    const query = queryString.stringify(params)

    return fetch(`${__OWM_API_URL__}/data/2.5/box/city?${query}`)
      .then(response => {
        if (response.status === 200) {
          return response.json()
        } else {
          throw new Error(response.statusText)
        }
      })
      .then(response => {
        dispatch(boundsWeatherLoaded(response))
      })
      .catch(error => {
        console.log(error)
      })
  }
}
