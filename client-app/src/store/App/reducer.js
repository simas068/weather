import {
  SHOW_SIDEBAR,
  HIDE_SIDEBAR,
  CITIES_LOADED,
  SAVE_GEOLOCATION
} from './constants'
import update from 'immutability-helper'

const initialState = {
  showSidebar: false,
  cities: {},
  userGeolocation: null
}

export default function appReducer (state = initialState, action) {
  switch (action.type) {
    case SHOW_SIDEBAR:
      return update(state, {
        $merge: {
          showSidebar: true
        }
      })
    case HIDE_SIDEBAR:
      return update(state, {
        $merge: {
          showSidebar: false
        }
      })
    case CITIES_LOADED:
      return update(state, {
        $merge: {
          cities: action.payload
        }
      })
    case SAVE_GEOLOCATION:
      return update(state, {
        $merge: {
          userGeolocation: action.payload
        }
      })
    default:
      return state
  }
}
