import { SHOW_SIDEBAR } from '../constants'

export function showSidebar () {
  return {
    type: SHOW_SIDEBAR
  }
}
