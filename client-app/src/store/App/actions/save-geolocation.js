import { SAVE_GEOLOCATION } from '../constants'

export function saveGeolocation (geolocation) {
  return {
    type: SAVE_GEOLOCATION,
    payload: geolocation
  }
}
