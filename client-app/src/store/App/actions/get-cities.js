import { citiesLoaded } from '../utils'
import queryString from 'query-string'
import { API_URL } from '../../../utils/constants'

export function getCities (countryId) {
  return (dispatch, getState) => {
    const params = {
      country: countryId
    }
    const query = queryString.stringify(params)

    return fetch(`${API_URL}/cities?${query}`)
      .then(response => {
        if (response.status === 200) {
          return response.json()
        } else {
          throw new Error(response.statusText)
        }
      })
      .then(response => {
        dispatch(
          citiesLoaded({
            [countryId]: response
          })
        )
      })
      .catch(error => {
        console.log(error)
      })
  }
}
