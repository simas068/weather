import { HIDE_SIDEBAR } from '../constants'

export function hideSidebar () {
  return {
    type: HIDE_SIDEBAR
  }
}
