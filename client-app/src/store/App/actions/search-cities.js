import queryString from 'query-string'
import { API_URL } from '../../../utils/constants'

export function searchCities (searchValue) {
  return (dispatch, getState) => {
    const params = {
      query: searchValue
    }
    const query = queryString.stringify(params)

    return fetch(`${API_URL}/cities/search?${query}`)
      .then(response => {
        if (response.status === 200) {
          return response.json()
        } else {
          throw new Error(response.statusText)
        }
      })
      .then(response => {
        return response
      })
      .catch(error => {
        console.log(error)
      })
  }
}
