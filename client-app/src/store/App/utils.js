import {
  CITIES_LOADED
} from './constants'

export function citiesLoaded (data) {
  return {
    type: CITIES_LOADED,
    payload: data
  }
}
