import { combineReducers } from 'redux'
import appReducer from './App/reducer'
import userReducer from './User/reducer'
import weatherReducer from './Weather/reducer'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    ...asyncReducers,
    app: appReducer,
    user: userReducer,
    weather: weatherReducer
  })
}

export default makeRootReducer
