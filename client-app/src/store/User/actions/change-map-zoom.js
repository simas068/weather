import { CHANGE_MAP_ZOOM } from '../constants'

export function changeMapZoom (zoom) {
  return {
    type: CHANGE_MAP_ZOOM,
    payload: zoom
  }
}
