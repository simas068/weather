import { REMOVE_FAVORITE_CITY } from '../constants'

export function removeFavoriteCity (city) {
  return {
    type: REMOVE_FAVORITE_CITY,
    payload: city
  }
}
