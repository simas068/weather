import { CHANGE_MAP_CENTER } from '../constants'

export function changeMapCenter (lat, lng) {
  return {
    type: CHANGE_MAP_CENTER,
    payload: {
      lat: lat,
      lng: lng
    }
  }
}
