import { ADD_FAVORITE_CITY } from '../constants'

export function addFavoriteCity (city) {
  return {
    type: ADD_FAVORITE_CITY,
    payload: city
  }
}
