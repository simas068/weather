import {
  CHANGE_MAP_CENTER,
  CHANGE_MAP_ZOOM,
  ADD_FAVORITE_CITY,
  REMOVE_FAVORITE_CITY
} from './constants'
import update from 'immutability-helper'

export const initialState = {
  lat: 54.6872,
  lng: 25.2797,
  zoom: 8,
  showSidebar: true,
  favoriteCities: []
}

function storeAppData (data) {
  localStorage.setItem('_user', JSON.stringify(data))
  return data
}

export default function userReducer (state = initialState, action) {
  if (!state) {
    state = initialState
  }
  switch (action.type) {
    case CHANGE_MAP_CENTER:
      return storeAppData(
        update(state, {
          $merge: action.payload
        })
      )
    case CHANGE_MAP_ZOOM:
      return storeAppData(
        update(state, {
          $merge: {
            zoom: action.payload
          }
        })
      )
    case ADD_FAVORITE_CITY:
      return storeAppData(
        update(state, {
          $merge: {
            favoriteCities: [ ...state.favoriteCities, action.payload ]
          }
        })
      )
    case REMOVE_FAVORITE_CITY:
      return storeAppData(
        update(state, {
          $merge: {
            favoriteCities: state.favoriteCities.filter(e => e !== action.payload)
          }
        })
      )
    default:
      return state
  }
}
