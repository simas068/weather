import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import CoreLayout from '../layouts/CoreLayout'
import Map from '../components/Map'
import Menu from '../components/Menu'

class AppContainer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      map: null
    }
  }

  onMapMounted = (ref) => {
    this.setState({
      map: ref
    })
  }

  render () {
    const { store } = this.props

    return (
      <Provider store={store}>
        <CoreLayout>
          <Map
            onMapMounted={this.onMapMounted}
          />
          <Menu
            mapRef={this.state.map}
          />
        </CoreLayout>
      </Provider>
    )
  }
}

AppContainer.propTypes = {
  store: PropTypes.object.isRequired
}

export default AppContainer
