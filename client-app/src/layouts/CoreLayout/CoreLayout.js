import PropTypes from 'prop-types'
import React from 'react'
import './CoreLayout.scss'

export const CoreLayout = ({ children }) => (
  <div className='coreLayout__wrapper'>
    <div className='coreLayout__container'>
      {children}
    </div>
  </div>
)

CoreLayout.propTypes = {
  children : PropTypes.node
}

export default CoreLayout
