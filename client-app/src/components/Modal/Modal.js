import React from 'react'
import ReactDOM from 'react-dom'
import './Modal.scss'
import PropTypes from 'prop-types'
const modalRoot = document.getElementById('modal-root')

class Modal extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      animateIn: false,
      animateOut: false
    }
    this.el = document.createElement('div')
    this.animationDuration = 200
  }

  componentDidMount = () => {
    this.updateModal()
  }

  componentWillUnmount = () => {
    this.unmountContainer()
  }

  componentDidUpdate = () => {
    if (this.props.visible !== this.previousVisible) {
      this.updateModal()
    }
  }

  animateIn = (callback) => {
    this.setState({
      visible: true,
      animateIn: true
    }, () => {
      setTimeout(() => {
        this.setState({
          animateIn: false
        }, callback)
      }, this.animationDuration)
    })
  }

  animateOut = (callback) => {
    this.setState({
      animateOut: true
    }, () => {
      setTimeout(() => {
        this.setState({
          visible: false,
          animateOut: false
        }, () => {
          callback()
        })
      }, this.animationDuration)
    })
  }

  mountContainer = () => {
    if (!this.drawerContainer) {
      this.drawerContainer = modalRoot.appendChild(this.el)
      if (!this.previousVisible) {
        this.animateIn()
      }
    }
  }

  unmountContainer = () => {
    if (this.drawerContainer) {
      this.animateOut(() => {
        modalRoot.removeChild(this.drawerContainer)
        delete this.drawerContainer
        if (this.props.afterClose) {
          this.props.afterClose()
        }
      })
    }
  }

  updateModal = () => {
    if (this.props.visible) {
      this.mountContainer()
    } else {
      this.unmountContainer()
    }
    this.previousVisible = this.props.visible
  }

  onCloseClick = (e) => {
    e.stopPropagation()
    if (this.props.onCloseClick) {
      this.props.onCloseClick()
    }
  }

  render = () => {
    const width = this.props.width > window.innerWidth ? window.innerWidth : this.props.width
    return (
      this.state.visible ? (
        ReactDOM.createPortal(
          <div
            className={`modal__wrapper modal__animation
            ${this.state.animateIn ? ' open' : ' '}
            ${this.state.animateOut ? ' close' : ' '}
          `}
          >
            <div className='modal__mask' />
            <div
              className='modal__container'
              onClick={this.onCloseClick}
            >
              <div
                className='modal__container-inner'
                onClick={(e) => { e.stopPropagation() }}
                style={{ width: width + 'px' }}
              >
                <div className='modal__container-header'>
                  <div className='modal__container-header-title'>
                    {this.props.title}
                  </div>
                  <i
                    className='modal__container-header-close fa fa-times'
                    onClick={this.onCloseClick}
                  />
                </div>
                <hr className='modal__divider' />
                <div className='modal__container-content'>
                  {this.props.children}
                </div>
              </div>
            </div>
          </div>
          , this.el)
      ) : null
    )
  }
}

Modal.propTypes = {
  children: PropTypes.node,
  visible: PropTypes.bool,
  afterClose: PropTypes.func,
  onCloseClick: PropTypes.func,
  title: PropTypes.string,
  width: PropTypes.number
}

Modal.defaultProps = {
  width: 520
}

export default Modal
