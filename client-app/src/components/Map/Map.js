import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import './Map.scss'
import GoogleMapWrapper from './components/GoogleMapWrapper'
import { changeMapCenter, changeMapZoom } from '../../store/User/actions'
import { getBoundsWeather } from '../../store/Weather/actions'
import WeatherNode from './components/WeatherNode'
import mapStyle from './map-style'

class Map extends React.Component {
  onCenterChanged = () => {
    const center = this.map.getCenter()
    this.props.dispatch(changeMapCenter(center.lat(), center.lng()))
  }

  onZoomChanged = () => {
    this.props.dispatch(changeMapZoom(this.map.getZoom()))
  }

  onBoundsChanged = () => {
    this.loadWeather()
  }

  loadWeather = () => {
    const bounds = this.map.getBounds()
    const zoom = this.map.getZoom()
    this.props.dispatch(getBoundsWeather(bounds, zoom))
  }

  onMapMounted = (ref) => {
    this.map = ref
    if (this.props.onMapMounted) {
      this.props.onMapMounted(ref)
    }
  }

  render () {
    const lat = this.props.user.lat || 0
    const lng = this.props.user.lng || 0
    const zoom = this.props.user.zoom || 8
    return (
      <div className='map__wrapper'>
        <GoogleMapWrapper
          defaultOptions={{
            mapTypeControl: false,
            streetViewControl: false,
            zoomControl: false,
            fullscreenControl: false,
            styles: mapStyle
          }}
          defaultZoom={zoom}
          defaultCenter={{ lat: lat, lng: lng }}
          containerElement={<div style={{ height: `100%` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          onCenterChanged={this.onCenterChanged}
          onZoomChanged={this.onZoomChanged}
          onMapMounted={this.onMapMounted}
          onBoundsChanged={this.onBoundsChanged}
        >
          {this.props.weather.bounds.list.map((item, i) =>
            <WeatherNode
              key={i}
              node={item}
            />
          )}
        </GoogleMapWrapper>
      </div>
    )
  }
}

Map.propTypes = {
  user: PropTypes.object,
  dispatch: PropTypes.func,
  weather: PropTypes.object,
  onMapMounted: PropTypes.func
}

const mapStateToProps = (state, props) => {
  return {
    user: state.user,
    weather: state.weather
  }
}

export default connect(mapStateToProps)(Map)
