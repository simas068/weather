import PropTypes from 'prop-types'
import React from 'react'
import { withGoogleMap, GoogleMap } from 'react-google-maps'

const GoogleMapWrapper = withGoogleMap(props => (
  <GoogleMap {...props} ref={props.onMapMounted}>
    {props.children}
  </GoogleMap>
))

GoogleMapWrapper.propTypes = {
  onMapMounted: PropTypes.func
}

export default GoogleMapWrapper
