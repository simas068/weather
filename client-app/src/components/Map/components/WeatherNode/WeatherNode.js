import PropTypes from 'prop-types'
import React from 'react'
import './WeatherNode.scss'
import { OverlayView } from 'react-google-maps'
import WeatherData from '../../../../components/WeatherData'
import Modal from '../../../../components/Modal'

const getPixelPositionOffset = (width, height) => ({
  x: -(width / 2),
  y: -(height / 2)
})

class WeatherNode extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showWeatherModal: false
    }
  }

  showWeatherModal = () => {
    this.setState({
      showWeatherModal: true
    })
  }

  hideWeatherModal = () => {
    this.setState({
      showWeatherModal: false
    })
  }

  render () {
    const { node } = this.props
    return (
      <OverlayView
        position={{ lat: node.coord.Lat, lng: node.coord.Lon }}
        mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
        getPixelPositionOffset={getPixelPositionOffset}
      >
        <div
          className='weatherNode__wrapper'
          onClick={this.showWeatherModal}
        >
          <div className='weatherNode__temperature'>
            {Math.round(node.main.temp)}&#176;
          </div>
          <div className='weatherNode__city'>
            {node.name}
          </div>
          <Modal
            visible={this.state.showWeatherModal}
            title={node.name}
            onCloseClick={this.hideWeatherModal}
          >
            <WeatherData
              lat={parseFloat(node.coord.Lat)}
              lng={parseFloat(node.coord.Lon)}
            />
          </Modal>
        </div>
      </OverlayView>
    )
  }
}

WeatherNode.propTypes = {
  node: PropTypes.object
}

export default WeatherNode
