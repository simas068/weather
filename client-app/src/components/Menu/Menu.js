import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import './Menu.scss'
import Drawer from '../../components/Drawer'
import Continents from '../../components/Continents'
import CitySearch from '../../components/CitySearch'
import FavoriteCities from '../../components/FavoriteCities'
import { changeMapCenter } from '../../store/User/actions'
import { saveGeolocation } from '../../store/App/actions'

class Menu extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      continentsVisible: false,
      searchVisible: false,
      favoritesVisible: false
    }
  }

  openContinentsDrawer = () => {
    this.setState({
      continentsVisible: true
    })
  }

  closeContinentsDrawer = () => {
    this.setState({
      continentsVisible: false
    })
  }

  openSearchDrawer = () => {
    this.setState({
      searchVisible: true
    })
  }

  closeSearchDrawer = () => {
    this.setState({
      searchVisible: false
    })
  }

  openFavoritesDrawer = () => {
    this.setState({
      favoritesVisible: true
    })
  }

  closeFavoritesDrawer = () => {
    this.setState({
      favoritesVisible: false
    })
  }

  isGeolocationAvailable = () => {
    return 'geolocation' in navigator
  }

  findMyLocation = () => {
    if (!this.props.userGeolocation) {
      navigator.geolocation.watchPosition(
        position => {
          if (!this.props.userGeolocation) {
            this.props.dispatch(saveGeolocation(position))
            this.setMapLocation(position.coords.latitude, position.coords.longitude)
          }
        },
        null,
        {
          enableHighAccuracy: true
        }
      )
    } else {
      this.setMapLocation(
        this.props.userGeolocation.coords.latitude,
        this.props.userGeolocation.coords.longitude
      )
    }
  }

  setMapLocation = (lat, lng) => {
    this.props.dispatch(changeMapCenter(lat, lng))
    this.props.mapRef.panTo({
      lat: lat,
      lng: lng
    })
  }

  render = () => {
    return (
      <div className='menu__wrapper'>
        <i
          title='Catalogue'
          className='menu__continents fa fa-globe fa-2x'
          onClick={this.openContinentsDrawer}
        />
        <i
          title='Search'
          className='menu__search fa fa-search fa-2x'
          onClick={this.openSearchDrawer}
        />
        <i
          title='Favorites'
          className='menu__favorites fa fa-star fa-2x'
          onClick={this.openFavoritesDrawer}
        />
        {this.isGeolocationAvailable() &&
          <i
            title='Find my location'
            className='menu__location fa fa-compass fa-2x'
            onClick={this.findMyLocation}
          />
        }
        <Drawer
          title='Select Continent'
          visible={this.state.continentsVisible}
          onCloseClick={this.closeContinentsDrawer}
        >
          <Continents
            mapRef={this.props.mapRef}
            onClose={this.closeContinentsDrawer}
          />
        </Drawer>
        <Drawer
          title='City Search'
          visible={this.state.searchVisible}
          onCloseClick={this.closeSearchDrawer}
        >
          <CitySearch
            mapRef={this.props.mapRef}
            onClose={this.closeSearchDrawer}
          />
        </Drawer>
        <Drawer
          title='Favorite cities'
          visible={this.state.favoritesVisible}
          onCloseClick={this.closeFavoritesDrawer}
        >
          <FavoriteCities
            mapRef={this.props.mapRef}
            onClose={this.closeFavoritesDrawer}
          />
        </Drawer>
      </div>
    )
  }
}

Menu.propTypes = {
  mapRef: PropTypes.object,
  dispatch: PropTypes.func,
  userGeolocation: PropTypes.object
}

const mapStateToProps = (state, props) => {
  return {
    userGeolocation: state.app.userGeolocation
  }
}

export default connect(mapStateToProps)(Menu)
