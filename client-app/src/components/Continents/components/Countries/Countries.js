import PropTypes from 'prop-types'
import React from 'react'
import './Countries.scss'
import { countries } from 'countries-list'
import Drawer from '../../../../components/Drawer'
import Cities from './components/Cities'

class Countries extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      countryId: null
    }
  }

  getCountriesOfContinent = (continentId) => {
    return Object.keys(countries).map(key => {
      return {
        _id: key,
        ...countries[key]
      }
    })
      .filter(c => c.continent === continentId)
      .sort((a, b) => {
        return a.name < b.name ? -1 : 1
      })
  }

  openCities = (countryId) => {
    this.setState({
      countryId: countryId
    })
  }

  closeCities = () => {
    this.setState({
      countryId: null
    })
  }

  onCitiesClose = () => {
    this.closeCities()
    if (this.props.onClose) {
      this.props.onClose()
    }
  }

  render = () => {
    const countriesOfContinent = this.getCountriesOfContinent(this.props.continentId)
    const chosenCountry = countriesOfContinent.find(i => i._id === this.state.countryId)
    return (
      <div className='countries__wrapper'>
        <ul>
          {countriesOfContinent.map(c =>
            <li
              key={c._id}
              onClick={() => { this.openCities(c._id) }}
            >
              {c.name}
            </li>
          )}
        </ul>
        <Drawer
          title={chosenCountry ? chosenCountry.name : null}
          visible={!!this.state.countryId}
          onCloseClick={this.closeCities}
        >
          <Cities
            countryId={this.state.countryId}
            mapRef={this.props.mapRef}
            onClose={this.onCitiesClose}
          />
        </Drawer>
      </div>
    )
  }
}

Countries.propTypes = {
  continentId: PropTypes.string,
  mapRef: PropTypes.object,
  onClose: PropTypes.func
}

export default Countries
