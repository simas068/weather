import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import './Cities.scss'
import City from '../../../../../City'
import { getCities } from '../../../../../../store/App/actions'

class Cities extends React.Component {
  componentDidMount = () => {
    this.loadCities(this.props.countryId)
  }

  loadCities = (countryId) => {
    if (!this.props.cities[countryId]) {
      this.props.dispatch(getCities(countryId))
    }
  }

  getCitiesOfCountry = (countryId) => {
    return this.props.cities[countryId] || []
  }

  render = () => {
    const citiesOfCountry = this.getCitiesOfCountry(this.props.countryId)
    return (
      <div className='cities__wrapper'>
        <ul>
          {citiesOfCountry.map((city, i) =>
            <li
              key={i}
            >
              <City
                city={city}
                mapRef={this.props.mapRef}
                showAddToFavorites
                onShowOnMapClick={this.props.onClose}
              />
            </li>
          )}
        </ul>
      </div>
    )
  }
}

Cities.propTypes = {
  countryId: PropTypes.string,
  mapRef: PropTypes.object,
  cities: PropTypes.object,
  dispatch: PropTypes.func,
  onClose: PropTypes.func
}

const mapStateToProps = (state, props) => {
  return {
    cities: state.app.cities
  }
}

export default connect(mapStateToProps)(Cities)
