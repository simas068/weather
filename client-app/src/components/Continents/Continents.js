import PropTypes from 'prop-types'
import React from 'react'
import './Continents.scss'
import Drawer from '../../components/Drawer'
import { continents } from 'countries-list'
import Countries from './components/Countries'

class Continents extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      continentId: null
    }
  }

  openCountries = (continentId) => {
    this.setState({
      continentId: continentId
    })
  }

  closeCountries = () => {
    this.setState({
      continentId: null
    })
  }

  onCountriesClose = () => {
    this.closeCountries()
    if (this.props.onClose) {
      this.props.onClose()
    }
  }

  render = () => {
    return (
      <div className='continents__wrapper'>
        <ul>
          {Object.keys(continents).map(key =>
            <li
              key={key}
              onClick={() => { this.openCountries(key) }}
            >
              {continents[key]}
            </li>
          )}
        </ul>
        <Drawer
          title={continents[this.state.continentId] ? continents[this.state.continentId] : null}
          visible={!!this.state.continentId}
          onCloseClick={this.closeCountries}
        >
          <Countries
            continentId={this.state.continentId}
            mapRef={this.props.mapRef}
            onClose={this.onCountriesClose}
          />
        </Drawer>
      </div>
    )
  }
}

Continents.propTypes = {
  mapRef: PropTypes.object,
  onClose: PropTypes.func
}

export default Continents
