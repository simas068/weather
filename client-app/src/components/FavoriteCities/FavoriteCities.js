import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import './FavoriteCities.scss'
import City from '../City'

const FavoriteCities = ({ favoriteCities, mapRef, onClose }) => (
  <div className='favoriteCities__wrapper'>
    <ul>
      {favoriteCities.map((city, i) =>
        <li
          key={i}
        >
          <City
            city={city}
            mapRef={mapRef}
            showRemoveFromFavorites
            showCountryName
            onShowOnMapClick={onClose}
          />
        </li>
      )}
    </ul>
  </div>
)

FavoriteCities.propTypes = {
  mapRef: PropTypes.object,
  favoriteCities: PropTypes.array,
  onClose: PropTypes.func
}

const mapStateToProps = (state, props) => {
  return {
    favoriteCities: state.user.favoriteCities
  }
}

export default connect(mapStateToProps)(FavoriteCities)
