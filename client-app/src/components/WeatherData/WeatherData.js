import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import './WeatherData.scss'
import { getGeoWeather } from '../../store/Weather/actions'

class WeatherData extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      dataLoaded: false,
      data: false
    }
  }

  componentDidMount = () => {
    this.loadWeatherData()
  }

  loadWeatherData = () => {
    this.props.dispatch(getGeoWeather(this.props.lat, this.props.lng))
      .then(response => {
        this.onWeatherDataLoaded(response)
      })
  }

  onWeatherDataLoaded = (data) => {
    this.setState({
      dataLoaded: true,
      data: data
    })
  }

  render = () => {
    const { dataLoaded, data } = this.state
    return (
      <div className='weatherData__wrapper'>
        {dataLoaded && data ? (
          <div>
            <div className='weatherData__row'>
              <div className='weatherData__row-key'>Weather Conditions</div>
              <div className='weatherData__row-value'>{data.weather[0].main}</div>
            </div>
            <div className='weatherData__row'>
              <div className='weatherData__row-key'>Temperature</div>
              <div className='weatherData__row-value'>{Math.round(data.main.temp)}&#176;</div>
            </div>
            <div className='weatherData__row'>
              <div className='weatherData__row-key'>Humidity</div>
              <div className='weatherData__row-value'>{data.main.humidity}%</div>
            </div>
            <div className='weatherData__row'>
              <div className='weatherData__row-key'>Wind Speed</div>
              <div className='weatherData__row-value'>{data.wind.speed}m/s</div>
            </div>
          </div>
        ) : null}
        {!dataLoaded &&
          <div className='weatherData__loading'>
            <i
              className='modal__container-header-close fa fa-spinner fa-2x fa-spin'
              onClick={this.onCloseClick}
            />
          </div>
        }
      </div>
    )
  }
}

WeatherData.propTypes = {
  lat: PropTypes.number,
  lng: PropTypes.number,
  dispatch: PropTypes.func
}

export default connect()(WeatherData)
