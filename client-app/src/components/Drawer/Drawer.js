import React from 'react'
import ReactDOM from 'react-dom'
import './Drawer.scss'
import PropTypes from 'prop-types'
const modalRoot = document.getElementById('modal-root')

class Drawer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      animateIn: false,
      animateOut: false
    }
    this.el = document.createElement('div')
    this.animationDuration = 200
  }

  componentDidMount = () => {
    this.updateModal()
  }

  componentWillUnmount = () => {
    this.unmountContainer()
  }

  componentDidUpdate = () => {
    if (this.props.visible !== this.previousVisible) {
      this.updateModal()
    }
  }

  animateIn = (callback) => {
    this.setState({
      visible: true,
      animateIn: true
    }, () => {
      setTimeout(() => {
        this.setState({
          animateIn: false
        }, callback)
      }, this.animationDuration)
    })
  }

  animateOut = (callback) => {
    this.setState({
      animateOut: true
    }, () => {
      setTimeout(() => {
        this.setState({
          visible: false,
          animateOut: false
        }, () => {
          callback()
        })
      }, this.animationDuration)
    })
  }

  mountContainer = () => {
    if (!this.drawerContainer) {
      this.drawerContainer = modalRoot.appendChild(this.el)
      if (!this.previousVisible) {
        this.animateIn()
      }
    }
  }

  unmountContainer = () => {
    if (this.drawerContainer) {
      this.animateOut(() => {
        modalRoot.removeChild(this.drawerContainer)
        delete this.drawerContainer
        if (this.props.afterClose) {
          this.props.afterClose()
        }
      })
    }
  }

  updateModal = () => {
    if (this.props.visible) {
      this.mountContainer()
    } else {
      this.unmountContainer()
    }
    this.previousVisible = this.props.visible
  }

  onCloseClick = () => {
    if (this.props.onCloseClick) {
      this.props.onCloseClick()
    }
  }

  render = () => {
    return (
      this.state.visible ? (
        ReactDOM.createPortal(
          <div
            className={`drawer__wrapper drawer__animation
            ${this.state.animateIn ? ' open' : ' '}
            ${this.state.animateOut ? ' close' : ' '}
          `}
          >
            <div className='drawer__container'>
              <div className='drawer__container-header'>
                <div className='drawer__container-header-title'>
                  {this.props.title}
                </div>
                <i
                  className='drawer__container-header-close fa fa-chevron-left fa-2x'
                  onClick={this.onCloseClick}
                />
              </div>
              <hr className='drawer__divider' />
              <div className='drawer__container-content'>
                {this.props.children}
              </div>
            </div>
          </div>
          , this.el)
      ) : null
    )
  }
}

Drawer.propTypes = {
  children: PropTypes.node,
  visible: PropTypes.bool,
  afterClose: PropTypes.func,
  onCloseClick: PropTypes.func,
  title: PropTypes.string
}

export default Drawer
