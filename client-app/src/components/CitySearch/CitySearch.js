import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import './CitySearch.scss'
import City from '../City'
import { searchCities } from '../../store/App/actions'

class CitySearch extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      searchResults: []
    }
  }

  onSearchValueChange = (e) => {
    this.loadSearchResults(e.target.value)
  }

  loadSearchResults = (searchValue) => {
    this.props.dispatch(searchCities(searchValue))
      .then(response => {
        this.setState({
          searchResults: response || []
        })
      })
  }

  render = () => {
    return (
      <div className='citySearch__wrapper'>
        <div className='citySearch__input'>
          <input
            autoFocus
            onChange={this.onSearchValueChange}
            placeholder='Enter city...'
          />
        </div>
        <div className='citySearch__results'>
          <ul>
            {this.state.searchResults.map((city, i) =>
              <li
                key={i}
              >
                <City
                  city={city}
                  mapRef={this.props.mapRef}
                  showAddToFavorites
                  showCountryName
                  onShowOnMapClick={this.props.onClose}
                />
              </li>
            )}
          </ul>
        </div>
      </div>
    )
  }
}

CitySearch.propTypes = {
  mapRef: PropTypes.object,
  dispatch: PropTypes.func,
  onClose: PropTypes.func
}

export default connect()(CitySearch)
