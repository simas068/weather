import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import './City.scss'
import Modal from '../../components/Modal'
import WeatherData from '../../components/WeatherData'
import { addFavoriteCity, removeFavoriteCity } from '../../store/User/actions'
import { countries } from 'countries-list'

class City extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showCityModal: false
    }
  }

  showOnMap = () => {
    if (this.props.mapRef) {
      this.props.mapRef.panTo({
        lat: parseFloat(this.props.city.lat),
        lng: parseFloat(this.props.city.lng)
      })
    }
    if (this.props.onShowOnMapClick) {
      this.props.onShowOnMapClick()
    }
  }

  showCityModal = () => {
    this.setState({
      showCityModal: true
    })
  }

  hideCityModal = () => {
    this.setState({
      showCityModal: false
    })
  }

  addToFavorites = () => {
    if (!this.isFavorite()) {
      this.props.dispatch(addFavoriteCity(this.props.city))
    }
  }

  removeFromFavorites = () => {
    this.props.dispatch(removeFavoriteCity(this.props.city))
  }

  isFavorite = () => {
    return this.props.favoriteCities.filter(e => JSON.stringify(e) === JSON.stringify(this.props.city)).length > 0
  }

  getCountryName = () => {
    return countries[this.props.city.country].name
  }

  render = () => {
    const { city } = this.props
    const favorite = this.isFavorite()
    return (
      <div className='city__wrapper'>
        <div
          className='city__name'
          onClick={this.showCityModal}
          title='Show Weather Info'
        >
          {city.name}
          {this.props.showCountryName &&
            <div className='city__name-country'>
              {this.getCountryName()}
            </div>
          }
        </div>
        <div className='city__icons'>
          <i
            className='city__map fa fa-map-marked-alt fa'
            onClick={this.showOnMap}
            title='Show On Map'
          />
          {this.props.showAddToFavorites &&
            <i
              className={`city__map fa fa-star fa ${favorite ? 'active' : ''}`}
              onClick={this.addToFavorites}
              title={favorite ? 'Favorite City' : 'Add To Favorites'}
            />
          }
          {this.props.showRemoveFromFavorites &&
            <i
              className='city__map fa fa-trash fa'
              onClick={this.removeFromFavorites}
              title='Remove From Favorites'
            />
          }
        </div>
        <Modal
          visible={this.state.showCityModal}
          title={city.name}
          onCloseClick={this.hideCityModal}
        >
          <WeatherData
            lat={parseFloat(city.lat)}
            lng={parseFloat(city.lng)}
          />
        </Modal>
      </div>
    )
  }
}

City.propTypes = {
  city: PropTypes.object,
  mapRef: PropTypes.object,
  dispatch: PropTypes.func,
  favoriteCities: PropTypes.array,
  showAddToFavorites: PropTypes.bool,
  showRemoveFromFavorites: PropTypes.bool,
  showCountryName: PropTypes.bool,
  onShowOnMapClick: PropTypes.func
}

const mapStateToProps = (state, props) => {
  return {
    favoriteCities: state.user.favoriteCities
  }
}

export default connect(mapStateToProps)(City)
