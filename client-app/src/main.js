import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import createStore from './store/createStore'
import AppContainer from './containers/AppContainer'
import './styles/_base.scss'
import { initialState as userInitialState } from './store/User/reducer'

// ========================================================
// Store Instantiation
// ========================================================
const _user = localStorage.getItem('_user')
const initialState = { user: JSON.parse(_user) || userInitialState }
const store = createStore(initialState)

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root')

let render = () => {
  ReactDOM.render(
    <AppContainer store={store} />,
    MOUNT_NODE
  )
}

render()
