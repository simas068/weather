const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const cssnano = require('cssnano')
const autoprefixer = require('autoprefixer')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

const devMode = process.env.NODE_ENV !== 'production'
const env = process.env.NODE_ENV || 'development'
const OWM_API_URL = process.env.OWM_API_URL || 'https://api.openweathermap.org'
const OWM_APP_ID = process.env.OWM_APP_ID || 'd2da7388ec5388245904b9b5bce56e42'

const appHtmlPlugin = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  chunks: ['app', 'vendors']
})
const cssPlugin = new MiniCssExtractPlugin({
  filename: devMode ? '[name].css' : '[name].[hash].css',
  chunkFilename: devMode ? '[id].css' : '[id].[hash].css'
})
const postCssOptions = {
  plugins: () => [
    autoprefixer,
    cssnano({
      zindex: false,
      reduceIdents: false,
      preset: ['default', {
        discardComments : {
          removeAll: true
        },
        discardUnused: false,
        mergeIdents: false,
        safe: true,
        sourcemap: true
      }]
    })
  ]
}

const config = {
  entry: {
    app: './src/main.js'
  },
  output: {
    filename: '[name].[chunkhash].js'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
          minChunks: 2
        }
      }
    }
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude : /node_modules/,
      use: {
        loader: 'babel-loader'
      }
    }, {
      test: /\.s?[ac]ss$/,
      use: [
        devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
        { loader: 'css-loader', options: { importLoaders: 1 } },
        { loader: 'postcss-loader', options: postCssOptions },
        'sass-loader'
      ]
    }, {
      test: /\.ttf(\?.*)?$/,
      use: {
        loader: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/octet-stream'
      }
    }, {
      test: /\.eot(\?.*)?$/,
      use: {
        loader: 'file-loader?prefix=fonts/&name=[path][name].[ext]'
      }
    }, {
      test: /\.woff(\?.*)?$/,
      use: {
        loader: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff'
      }
    }, {
      test: /\.woff2(\?.*)?$/,
      use: {
        loader: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff2'
      }
    }, {
      test: /\.(png|jpg|svg)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 8192
        }
      }
    }]
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    cssPlugin,
    appHtmlPlugin,
    new webpack.DefinePlugin({
      'process.env'      : {
        'NODE_ENV'    : JSON.stringify(env)
      },
      'NODE_ENV'         : env,
      '__DEV__'          : env === 'development',
      '__PROD__'         : env === 'production',
      '__TEST__'         : env === 'test',
      '__OWM_API_URL__'  : JSON.stringify(OWM_API_URL),
      '__OWM_APP_ID__'   : JSON.stringify(OWM_APP_ID)
    }),
    new CopyWebpackPlugin([
      { context: 'src/static/', from: '**/*' }
    ])
  ]
}

module.exports = config
