var express = require('express')
var bodyParser = require('body-parser')
var cities = require('cities.json')

var app = express()
app.use(express.static('./client-app/dist'))
app.use(bodyParser.json())

var router = express.Router()

router.get('/cities', function(req, res) {
  if (!req.query.country) {
    res.json(cities)
    return
  }
  res.json(
    cities
      .filter(c => c.country === req.query.country)
      .sort((a, b) => {
        return a.name < b.name ? -1 : 1
      })
  )
})

router.get('/cities/search', function(req, res) {
  if (!req.query.query) {
    res.json([])
    return
  }
  res.json(
    cities
      .filter(c => c.name.toLowerCase().includes(req.query.query.toLowerCase()))
      .sort((a, b) => {
        return a.name < b.name ? -1 : 1
      })
      .slice(0, 10) || []
  )
})

app.use('/api', router)

var server = app.listen(process.env.PORT || 8080, function () {
  var port = server.address().port
  console.log('Server is now running on port', port)
})
