# Weather App

[Website](https://the-greatest-app.herokuapp.com/) |
[Stage](https://the-greatest-app-stage.herokuapp.com/) |
[Weather API](https://openweathermap.org/)

Website is hosted on free Heroku dyno so it takes a bit time to wake up on first request. Please be patient!

#### To run development server
```
$ git clone git@gitlab.com:simas068/weather.git
$ cd weather
$ npm install
$ npm run dev
```
```
$ cd client-app
$ npm run dev
```
Website is hosted on `localhost:8080`

#### To build client
```
$ cd client-app
$ npm run build
```

Built project is stored in `client-app/dist` directory.

#### Client lint test
```
$ cd client-app
$ npm run lint
$ npm run sass-lint
```
